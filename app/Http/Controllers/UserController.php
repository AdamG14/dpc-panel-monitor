<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Entity\History;
use App\Entity\User;
use App\Entity\Note;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::orderBy('id', 'DESC')->get();
        return view('admin.user.main', ['user' => $user]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userNotes()
    {
        $note = Note::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view('admin.user.notes.main', ['note' => $note]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'role' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        
        $user = new User;
        $user->name = $request->name;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        History::create([
            'table' => 'Account User',
            'action' => 'CREATE ' . $user->name,
            'user' => Auth::user()->name,
        ]);

        Session::flash('alert', 'Success create new account');
        return redirect('/admin/user/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.user.edit', ['user' => $user]);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'role' => 'required',
            'email' => 'required|email',
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->role = $request->role;
        $user->email = $request->email;
        if(!is_null($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        History::create([
            'table' => 'Account User',
            'action' => 'UPDATE '. $user->name,
            'user' => Auth::user()->name,
        ]);

        Session::flash('alert', 'Success update account');
        return redirect('/admin/user/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        
        History::create([
            'table' => 'Account User',
            'action' => 'DELETE '. $user->name,
            'user' => Auth::user()->name,
        ]);

        $user->delete();

        Session::flash('alert', 'Success delete account');
        return redirect('/admin/user/list');
    }
}
