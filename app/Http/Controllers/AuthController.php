<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Entity\Project;
use App\Entity\PurchaseOrder;
use App\Entity\Note;
use App\Entity\Guest;
use App\Entity\History;
use App\Entity\User;
use Carbon\Carbon;

class AuthController extends Controller
{
    /**
     * Display login
     */
    public function indexLogin()
    {
        return view('auth.login');
    }

    /**
     * Checking credentials
     */
    public function checkLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            if(Auth::user()->role == "Guest") {
                Guest::create([
                    'ip' => request()->ip(),
                ]);
            }
             
            return redirect('admin/main');
        } else {            
            return redirect()->back()->with('alert-fail','Email atau password salah!');
        }    
    }

    public function logout(){
        Auth::logout();
        return redirect('')->with('alert-logout','Berhasil Logout!');
    }

    /**
     * Display dashboard
     */
    public function indexDashboard()
    {
        $project    = Project::orderBy('created_at', 'desc')->get();
        $po         = PurchaseOrder::orderBy('date', 'desc')->get();
        $payment    = [];
        foreach($po as $pay) {
            if(is_null($pay->project['payment_date'])) array_push($payment, $pay);
        }
        $notes      = Note::orderBy('created_at', 'desc')->get();
        $user       = User::all();
        $ptValue    = 0;
        $cvValue    = 0;
        $ptValueJan = 0;$ptValueFeb = 0;$ptValueMar = 0;$ptValueApr = 0;$ptValueMei = 0;$ptValueJun = 0;$ptValueJul = 0;$ptValueAgu = 0;$ptValueSep = 0;$ptValueOkt = 0;$ptValueNov = 0;$ptValueDes = 0;
        $cvValueJan = 0;$cvValueFeb = 0;$cvValueMar = 0;$cvValueApr = 0;$cvValueMei = 0;$cvValueJun = 0;$cvValueJul = 0;$cvValueAgu = 0;$cvValueSep = 0;$cvValueOkt = 0;$cvValueNov = 0;$cvValueDes = 0;
        foreach($po as $p) {
            if($p->project['deliver_to'] == "PT. Dua Putra Cemerlang") {
                $value = str_replace('.', '', $p->value);
                $ptValue   += $value;

                if(Carbon::parse($p->date)->format('Y') == Carbon::now()->format('Y')) {
                    if(Carbon::parse($p->date)->format('m') == '1') $ptValueJan += $value;
                    if(Carbon::parse($p->date)->format('m') == '2') $ptValueFeb += $value;
                    if(Carbon::parse($p->date)->format('m') == '3') $ptValueMar += $value;
                    if(Carbon::parse($p->date)->format('m') == '4') $ptValueApr += $value;
                    if(Carbon::parse($p->date)->format('m') == '5') $ptValueMei += $value;
                    if(Carbon::parse($p->date)->format('m') == '6') $ptValueJun += $value;
                    if(Carbon::parse($p->date)->format('m') == '7') $ptValueJul += $value;
                    if(Carbon::parse($p->date)->format('m') == '8') $ptValueAgu += $value;
                    if(Carbon::parse($p->date)->format('m') == '9') $ptValueSep += $value;
                    if(Carbon::parse($p->date)->format('m') == '10') $ptValueOkt += $value;
                    if(Carbon::parse($p->date)->format('m') == '11') $ptValueNov += $value;
                    if(Carbon::parse($p->date)->format('m') == '12') $ptValueDes += $value;
                }
            }
            if($p->project['deliver_to'] == "CV. Dua Putra Cemerlang") {
                $value = str_replace('.', '', $p->value);
                if(Carbon::parse($p->date)->format('Y') == Carbon::now()->format('Y')) {
                    if(Carbon::parse($p->date)->format('m') == '1') $cvValueJan += $value;
                    if(Carbon::parse($p->date)->format('m') == '2') $cvValueFeb += $value;
                    if(Carbon::parse($p->date)->format('m') == '3') $cvValueMar += $value;
                    if(Carbon::parse($p->date)->format('m') == '4') $cvValueApr += $value;
                    if(Carbon::parse($p->date)->format('m') == '5') $cvValueMei += $value;
                    if(Carbon::parse($p->date)->format('m') == '6') $cvValueJun += $value;
                    if(Carbon::parse($p->date)->format('m') == '7') $cvValueJul += $value;
                    if(Carbon::parse($p->date)->format('m') == '8') $cvValueAgu += $value;
                    if(Carbon::parse($p->date)->format('m') == '9') $cvValueSep += $value;
                    if(Carbon::parse($p->date)->format('m') == '10') $cvValueOkt += $value;
                    if(Carbon::parse($p->date)->format('m') == '11') $cvValueNov += $value;
                    if(Carbon::parse($p->date)->format('m') == '12') $cvValueDes += $value;
                }
                $cvValue   += $value;
            }
        }

        $done       = Project::where('status', 'done')->get();
        $progress   = Project::where('status', 'progress')->get();
        $pending    = Project::where('status', 'pending')->get();

        $guest      = Guest::whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))->get()->count();
        $queryuser  = History::whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))->select('user')->get();
        $employee   = 0;
        $admin      = 0;
        $director   = 0;
        foreach($queryuser as $query) {
            $nameemp    = User::where('role', 'Employee')->where('name', $query->user)->first();
            if(!is_null($nameemp)) {
                $employee   += 1;
            }
            $nameadm    = User::where('role', 'Admin')->where('name', $query->user)->first();
            if(!is_null($nameadm)) {
                $admin      += 1;
            }
            $namedir    = User::where('role', 'Director')->where('name', $query->user)->first();
            if(!is_null($namedir)) {
                $director   += 1;
            }
        }
        
        return view('admin.dashboard.main', compact(['project', 'po' , 'notes', 'payment',
                                            'done', 'progress', 'pending',
                                            'guest', 'employee', 'admin', 'director',
                                            'ptValue', 'cvValue', 'user', 
                                            'ptValueJan', 'ptValueFeb', 'ptValueMar',
                                            'ptValueApr', 'ptValueMei', 'ptValueJun', 
                                            'ptValueJul', 'ptValueAgu', 'ptValueSep',
                                            'ptValueOkt', 'ptValueNov', 'ptValueDes', 
                                            'cvValueJan', 'cvValueFeb', 'cvValueMar',
                                            'cvValueApr', 'cvValueMei', 'cvValueJun', 
                                            'cvValueJul', 'cvValueAgu', 'cvValueSep',
                                            'cvValueOkt', 'cvValueNov', 'cvValueDes', ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
