<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entity\Note;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Note::class, function (Faker $faker) {
    static $number = 1;
    return [
        'title' => $faker->address,
        'detail' => $faker->name,
        'date' => Carbon::now(),
        'project_id' => $number++,
        'user_id' => 1,
    ];
});
