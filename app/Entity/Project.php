<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'location', 'project_owner', 'start_project', 'end_project', 'atp', 
        'start_bast', 'end_bast', 'invoice_date', 'payment_date', 'status', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Entity\User', 'user_id', 'id');
    }

}
