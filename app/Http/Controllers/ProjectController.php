<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Entity\Project;
use App\Entity\PurchaseOrder;
use App\Entity\History;
use App\Entity\User;
use Carbon\Carbon;

class ProjectController extends Controller
{
    /**
     * Display list of project
     */
    public function index()
    {
        $purchase = PurchaseOrder::all();
        $projects = Project::orderBy('id', 'DESC')->get();
        $user     = User::all();
        return view('admin.project.main', ['projects' => $projects, 'purchase' => $purchase, 'user' => $user]);
    }

    /**
     * Display log of action
     */
    public function indexLog()
    {
        $history = History::all();
        return view('admin.project.log', ['history' => $history]);
    }

    /**
     * Show the form for creating a new project
     */
    public function create()
    {
        return view('admin.project.create');
    }

    /**
     * Store a new created project
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'startProject' => 'required',
            'projectOwner' => 'required',
            'location' => 'required',
            'deliverTo' => 'required',
            'numberPO' => 'required',
            'valuePO' => 'required',
            'datePO' => 'required',
        ]);

        $project = new Project;
        $project->status = 1;
        $project->start_project = Carbon::parse($request->startProject)->format('Y-m-d');
        $project->end_project   = is_null($request->endProject) ? null : Carbon::parse($request->endProject)->format('Y-m-d');
        $project->project_owner = $request->projectOwner;
        $project->location      = $request->location;
        $project->deliver_to    = $request->deliverTo;
        $project->save();

        PurchaseOrder::create([
            'number' => Input::get('numberPO'),
            'value' => substr(Input::get('valuePO'), 4),
            'date' => Carbon::parse(Input::get('datePO'))->format('Y-m-d'),
            'project_id' => $project->id,
        ]);

        History::create([
            'table' => 'Project & PurchaseOrder',
            'action' => 'CREATE '. $project->location,
            'user' => Auth::user()->name,
        ]);

        Session::flash('alert', 'Success add new project');
        return redirect('/admin/project/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $purchase = PurchaseOrder::where('project_id', $id)->first();
        $project  = Project::find($id);

        if($request->typeShow != null) {
            if($request->typeShow == "sipil") {
                $imagesSipil        = explode(',', $project->image_sipil);
                $imagesElectrical[0]= "";
                $imagesPerijinan[0] = "";
            } elseif($request->typeShow == "electrical") {
                $imagesElectrical   = explode(',', $project->image_electrical);
                $imagesSipil[0]     = "";
                $imagesPerijinan[0] = "";
            } elseif($request->typeShow == "perijinan") {
                $imagesPerijinan    = explode(',', $project->image_perijinan);
                $imagesSipil[0]     = "";
                $imagesElectrical[0]= "";
            }
        } else {
            $imagesSipil        = explode(',', $project->image_sipil);
            $imagesElectrical   = explode(',', $project->image_electrical);
            $imagesPerijinan    = explode(',', $project->image_perijinan);
        }

        return view('admin.project.show')->with(compact('purchase', 'imagesSipil', 'imagesElectrical', 'imagesPerijinan', 'project'));
    }

    /**
     * Store a new document
     */
    public function storeDocument(Request $request, $id)
    {
        $this->validate($request, [
            'photos.*' => 'required|mimes:jpeg,jpg,png',
        ]);

        $project = Project::find($id);

        if($request->tipe == "sipil") {
                foreach($request->file('photos') as $image) {
                    $name = $image->getClientOriginalName();
                    $image->move(public_path('images/sipil/'), $name);

                    $project->image_sipil = is_null($project->image_sipil) ? $name : $project->image_sipil . ',' . $name;
                }
        }
        if($request->tipe == "electrical") {
            foreach($request->file('photos') as $image) {
                $name = $image->getClientOriginalName();
                $image->move(public_path('images/electrical/'), $name);

                $project->image_electrical = is_null($project->image_electrical) ? $name : $project->image_electrical . ',' . $name;
            }
        }
        if($request->tipe == "perijinan") {
            foreach($request->file('photos') as $image) {
                $name = $image->getClientOriginalName();
                $image->move(public_path('images/perijinan/'), $name);

                $project->image_perijinan = is_null($project->image_perijinan) ? $name : $project->image_perijinan . ',' . $name;
            }
        }        
        $project->save();
        
        Session::flash('alert', 'Success add new document');
        return redirect('/admin/document/'. $id);
    }

    /**
     * Show the form for editing the project
     */
    public function edit($id)
    {
        $project = Project::find($id);
        $purchase = PurchaseOrder::where('project_id', $id)->get();
        return view('admin.project.edit', ['project' => $project, 'purchase' => $purchase]);
    }

    /**
     * Update project table in database.
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
            'startProject' => 'required',
            'endProject' => 'required',
            'projectOwner' => 'required',
            'location' => 'required',
            'numberPO' => 'required',
            'valuePO' => 'required',
            'datePO' => 'required',
            'deliverTo' => 'required',
        ]);


        DB::transaction(function() {
            $id = Input::get('projectId');
            $location = Project::find($id);

            $project = Project::where('id', $id)->update([
                'status' => Input::get('status'),
                'start_project' => Carbon::parse(Input::get('startProject'))->format('Y-m-d'),
                'end_project' => Carbon::parse(Input::get('endProject'))->format('Y-m-d'),
                'project_owner' => Input::get('projectOwner'),
                'location' => Input::get('location'),
                'start_bast' => is_null(Input::get('startBast')) ? null : Carbon::parse(Input::get('startBast'))->format('Y-m-d'),
                'end_bast' => is_null(Input::get('endBast')) ? null : Carbon::parse(Input::get('endBast'))->format('Y-m-d'),
                'payment_date' => is_null(Input::get('paymentDate')) ? null : Carbon::parse(Input::get('paymentDate'))->format('Y-m-d'),
                'invoice_date' => is_null(Input::get('invoiceDate')) ? null :Carbon::parse(Input::get('invoiceDate'))->format('Y-m-d'),
                'atp' => is_null(Input::get('atp')) ? null : Carbon::parse(Input::get('atp'))->format('Y-m-d'),
                'deliver_to' => Input::get('deliverTo'),
            ]);
    
            PurchaseOrder::where('project_id', $id)->update([
                'number' => Input::get('numberPO'),
                'value' => substr(Input::get('valuePO'), 4),
                'date' => Carbon::parse(Input::get('datePO'))->format('Y-m-d'),
            ]);
    
            History::create([
                'table' => 'Project & PurchaseOrder',
                'action' => 'EDIT '. $location->location,
                'user' => Auth::user()->name,
            ]);    
        });
        
        Session::flash('alert', 'Success update project');
        return redirect('/admin/project/list');
    }

    /**
     * Remove the specified project from project table
     */
    public function destroy($id)
    {
        PurchaseOrder::where('project_id', $id)->delete();

        $project = Project::find($id);

        History::create([
            'table' => 'Project & PurchaseOrder',
            'action' => 'DELETE '. $project->location,
            'user' => Auth::user()->name,
        ]);  

        $project->delete();

        Session::flash('alert', 'Success delete project');
        return redirect('/admin/project/list');
    }
}
