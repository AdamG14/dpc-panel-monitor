<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Project
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> DPC</a></li>
        <li>Dashboard</li>
        <li class="active">Edit Project</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <form action="{{ route('updateProject', $project->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT')}}
          <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Project Date</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="">Start</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker" name="startProject" autocomplete="off" value="{{ \Carbon\Carbon::parse($project->start_project)->format('d F Y') }}" readonly>
                  </div>
                  @if($errors->has('startProject'))
                  <div class="text-danger">
                      {{ $errors->first('startProject')}}
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="">Finish</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker2" name="endProject" autocomplete="off" value="{{ \Carbon\Carbon::parse($project->end_project)->format('d F Y') }}" readonly>
                  </div>
                  @if($errors->has('endProject'))
                  <div class="text-danger">
                      {{ $errors->first('endProject')}}
                  </div>
                  @endif
                </div>
                <h4 class="box-title">Invoice Date</h3>
                <div class="form-group">
                  <label for="">Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker5" name="invoiceDate" autocomplete="off" value="{{ is_null($project->invoice_date) ? '' : \Carbon\Carbon::parse($project->invoice_date)->format('d F Y') }}" readonly>
                  </div>
                  @if($errors->has('invoiceDate'))
                  <div class="text-danger">
                      {{ $errors->first('invoiceDate')}}
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Project Identity</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="">Project Owner</label>
                  <input type="text" class="form-control" placeholder="Project Owner..." name="projectOwner" autocomplete="off" value="{{ $project->project_owner }}">
                  <input type="hidden" class="form-control" name="projectId" autocomplete="off" value="{{ $project->id }}">
                  @if($errors->has('projectOwner'))
                  <div class="text-danger">
                      {{ $errors->first('projectOwner')}}
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="">Location</label>
                  <input type="text" class="form-control" placeholder="Location..." name="location" autocomplete="off" value="{{ $project->location }}">
                  @if($errors->has('location'))
                  <div class="text-danger">
                      {{ $errors->first('location')}}
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="">Status</label>
                  <select name="status" class="form-control">
                      <option value="1" {{ $project->status == 'Pending' ? 'selected' : ''}}>Pending</option>
                      <option value="2" {{ $project->status == 'Progress' ? 'selected' : ''}}>Progress</option>
                      <option value="3" {{ $project->status == 'Done' ? 'selected' : ''}}>Done</option>
                  </select>
                  @if($errors->has('status'))
                  <div class="text-danger">
                      {{ $errors->first('status')}}
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="">Delivered to</label>
                  <select name="deliverTo" class="form-control">
                    <option value="1" {{ $project->deliver_to === 'PT. Dua Putra Cemerlang Utama' ? 'selected' : ''}}>PT. Dua Putra Cemerlang Utama</option>
                    <option value="2" {{ $project->deliver_to === 'CV. Dua Putra Cemerlang' ? 'selected' : ''}}>CV. Dua Putra Cemerlang</option>
                  </select>
                  @if($errors->has('startProject'))
                  <div class="text-danger">
                      {{ $errors->first('startProject')}}
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">BAST Date</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="">Start</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker3" name="startBast" autocomplete="off" value="{{ is_null($project->start_bast) ? '' : \Carbon\Carbon::parse($project->start_bast)->format('d F Y') }}" readonly>
                  </div>
                  @if($errors->has('startBast'))
                  <div class="text-danger">
                      {{ $errors->first('startBast')}}
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="">Finish</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker4" name="endBast" autocomplete="off" value="{{ is_null($project->end_bast) ? '' :\Carbon\Carbon::parse($project->end_bast)->format('d F Y') }}" readonly>
                  </div>
                  @if($errors->has('endBast'))
                  <div class="text-danger">
                      {{ $errors->first('endBast')}}
                  </div>
                  @endif
                </div>
                <h4 class="box-title">Payment Date</h3>
                <div class="form-group">
                  <label for="">Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker6" name="paymentDate" autocomplete="off" value="{{ is_null($project->payment_date) ? '' :\Carbon\Carbon::parse($project->payment_date)->format('d F Y') }}" readonly>
                  </div>
                  @if($errors->has('paymentDate'))
                  <div class="text-danger">
                      {{ $errors->first('paymentDate')}}
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
      </div>
      <div class="row">
          <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">ATP</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="">Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker8" name="atp" autocomplete="off" value="{{ is_null($project->atp) ? '' : \Carbon\Carbon::parse($project->atp)->format('d F Y') }}" readonly>
                  </div>
                  @if($errors->has('atp'))
                  <div class="text-danger">
                      {{ $errors->first('atp')}}
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Purchase Order</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="">PO Number</label>
                  <input type="text" class="form-control" placeholder="PO Number..." name="numberPO" autocomplete="off" value="{{ $purchase[0]->number }}">
                  @if($errors->has('numberPO'))
                  <div class="text-danger">
                      {{ $errors->first('numberPO')}}
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="">PO Value</label>
                  <input type="text" class="form-control" id="rupiah" placeholder="PO Value..." name="valuePO" autocomplete="off" value="Rp. {{ $purchase[0]->value }}">
                  @if($errors->has('valuePO'))
                  <div class="text-danger">
                      {{ $errors->first('valuePO')}}
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="">PO Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker7" name="datePO" autocomplete="off" value="{{ \Carbon\Carbon::parse($purchase[0]->date)->format('d F Y') }}" readonly>
                  </div>
                  @if($errors->has('datePO'))
                  <div class="text-danger">
                      {{ $errors->first('datePO')}}
                  </div>
                  @endif
                </div>
                <button type="submit" class="btn btn-primary" style="margin-left:42.5%; margin-top: -4.8px;">Submit</button>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            </form>
          </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
  $('#datepicker').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
    orientation: "bottom"
  })
  $('#datepicker2').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
    orientation: "bottom"
  })
  $('#datepicker3').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
    orientation: "bottom"
  })
  $('#datepicker4').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
    orientation: "bottom"
  })
  $('#datepicker5').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
    orientation: "bottom"
  })
  $('#datepicker6').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
    orientation: "bottom"
  })
  $('#datepicker7').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
    orientation: "top"
  })
  $('#datepicker8').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
    orientation: "top"
  })
</script>
<script type="text/javascript">
		
		var rupiah = document.getElementById('rupiah');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp. ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}
	</script>
</body>
</html>