  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Developed by TermosDev
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Dua Putra Cemerlang</a>.</strong>
  </footer>