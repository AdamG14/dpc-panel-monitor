<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Project
      </h1>
      @if (Session::has('alert'))
      <div class="alert alert-success alert-dismissible" style="margin-top: 10px; margin-bottom: -10px;">
          <a href="admin/project/list"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert')}}
      </div>
      @endif
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> DPC</a></li>
        <li>Dashboard</li>
        <li class="active">Project</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-hover">
                  <thead>
                  <style>th{text-align: center;}</style>
                  <tr>
                    <th rowspan="2">Id</th>
                    <th rowspan="2"><div style="margin-bottom: 20px">Status</div></th>
                    <th rowspan="2"><div style="margin-bottom: 20px">Project</div></th>
                    <th rowspan="2"><div style="margin-bottom: 10px">Owner Project</div></th>
                    <th rowspan="2"><div style="margin-bottom: 20px">Delivered To</div></th>
                    <th colspan="2">PO</th>
                    <th colspan="2">Progress</th>     
                    <th rowspan="2"><div style="margin-bottom: 20px; margin-left: 15px;">ATP</div></th>
                    <th colspan="2">BAST</th>
                    <th rowspan="2"><div style="margin-bottom: 10px; margin-left: 20px;">Invoice Date</div></th>
                    <th rowspan="2"><div style="margin-bottom: 10px; margin-left: 20px;">Payment Date</div></th>
                    <th rowspan="2"></th>
                  </tr>
                  <tr>              
                    <th class="col-md-1">Value</th>                            
                    <th>Date</th>
                    <th>Start</th>
                    <th>Finish</th>            
                    <th>Submit</th>
                    <th style="border-right: 1px solid #f4f4f4;">Finish</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($projects as $p)
                  <tr>
                    <td>{{ $p->id }}</td>
                    @if($p->status == 'Pending')
                    <td><span class="label label-danger">{{ $p->status }}</span></td>
                    @elseif($p->status == 'Progress')
                    <td><span class="label label-primary">{{ $p->status }}</span></td>
                    @else
                    <td><span class="label label-success">{{ $p->status }}</span></td>
                    @endif
                    <td>{{ $p->location }}</td>
                    <td>{{ $p->project_owner }}</td>
                    <td>{{ $p->deliver_to }}</td>
                    @foreach($purchase as $po)
                    @if($po->project_id == $p->id)          
                    <td>Rp. {{ $po->value }}</td>
                    <td>{{ \Carbon\Carbon::parse($po->date)->format('d F Y') }}</td>
                    @endif
                    @endforeach
                    <td>{{ \Carbon\Carbon::parse($p->start_project)->format('d F Y') }}</td>
                    <td>{{ is_null($p->end_project) ? $p->end_project : \Carbon\Carbon::parse($p->end_project)->format('d F Y') }}</td>
                    <td>{{ is_null($p->atp) ? $p->atp : \Carbon\Carbon::parse($p->atp)->format('d F Y') }}</td>
                    <td>{{ is_null($p->start_bast) ? $p->start_bast : \Carbon\Carbon::parse($p->start_bast)->format('d F Y') }}</td>
                    <td>{{ is_null($p->end_bast) ? $p->end_bast : \Carbon\Carbon::parse($p->end_bast)->format('d F Y') }}</td>
                    <td>{{ is_null($p->invoice_date) ? $p->invoice_date : \Carbon\Carbon::parse($p->invoice_date)->format('d F Y') }}</td>                    
                    <td>{{ is_null($p->payment_date) ? $p->payment_date : \Carbon\Carbon::parse($p->payment_date)->format('d F Y') }}</td>
                    <td><a href="{{ route('showDocument', $p->id) }}" class="btn btn-i]nfo" style="margin: 3px;"><i class="fa fa-eye"></i></a>
                    @if(Auth::user()->role == 'Employee' || Auth::user()->role == 'Admin' || Auth::user()->role == 'Director')
                    <button type="button" class="btn btn-success" style="margin: 3px;" data-toggle="modal" data-target="#addNotes" onclick="create_notes('{{ $p->id }}')"><i class="fa fa-file"></i></button>
                    @endif
                    @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Director')
                    <a href="{{ route('editProject', $p->id) }}" class="btn btn-warning" style="margin: 3px;"><i class="fa fa-pencil"></i></a>
                    <button type="button" class="btn btn-success" style="margin: 3px;" data-toggle="modal" data-target="#addTasks" onclick="create_task('{{ $p->id }}')"><i class="fa fa-plus"></i></button>
                    @endif
                    @if(Auth::user()->role == 'Director')<a href="{{ route('deleteProject', $p->id) }}" class="btn btn-danger" style="margin: 3px;"><i class="fa fa-trash"></i></a></td>
                    @endif
                  </tr>
                  @endforeach
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->

<!-- Modal -->
<div class="modal fade" id="addNotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 30px; margin-top: 15%;">
      <div class="modal-header">
        <h1 class="modal-title" style="text-align: center;">Add New Notes</h1>
      </div>
      <div class="modal-body">
      <form method="post" id="form-create-notes">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="form-group">
          <label>Title</label>
          <input type="title" class="form-control" placeholder="Title note..." name="title">
        </div>
        <div class="form-group">
          <label>Detail</label>
          <textarea class="form-control" placeholder="Detail note..." rows="10" name="detail"></textarea>
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
      </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addTasks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 30px; margin-top: 10%;">
      <div class="modal-header">
        <h1 class="modal-title" style="text-align: center;">Add New Tasks</h1>
      </div>
      <div class="modal-body">
      <form method="post" id="form-create-task">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="form-group">
          <label>Add Task</label>
          <input type="text" class="form-control" style="margin-bottom: 10px;" name="task1" id="task1" autocomplete="off">
          <input type="text" class="form-control" style="margin-bottom: 10px;" name="task2" id="task2" autocomplete="off">
          <input type="text" class="form-control" style="margin-bottom: 10px;" name="task3" id="task3" autocomplete="off">
          <input type="text" class="form-control" style="margin-bottom: 10px;" name="task4" id="task4" autocomplete="off">
          <input type="text" class="form-control" name="task5" id="task5" autocomplete="off">
        </div>
        <div class="form-group">
          <label>Assigned To</label>
          <select name="userId" class="form-control">
            <option value="">Select Employee</option>
            @foreach($user as $u)
            <option value="{{ $u->id }}">{{ $u->name }}</option>
            @endforeach
          </select>
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
      </form>
      </div>
    </div>
  </div>
</div>

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
    $('#example1').DataTable( {
      "columnDefs" : [
        { 'visible': false, 'targets': [0] },
        { 'orderable': false, 'targets': [1, 2, 3, 4, 5, 14] }
      ],
      "order": [[ 0, "desc" ]],
      "paging": true
    });
  })

  function create_notes(id){
    $("#form-create-notes").attr("action", "/admin/notes/store/" + id);
  }

  function create_task(id){
    $("#form-create-task").attr("action", "/admin/task/store/" + id);
  }

  $('#task1, #task2, #task3, #task4, #task5').keydown(function(e){
   var ingnore_key_codes = [188];
   if ($.inArray(e.keyCode, ingnore_key_codes) >= 0){
      e.preventDefault();
   }
  });

  $(function(){

  $('#task1').bind('paste',function()
  {
      setTimeout(function()
      { 
        //get the value of the input text
        var data= $( '#task1' ).val() ;
        //replace the special characters to '' 
        var dataFull = data.replace(/,/g, '');
        //set the new value of the input text without special characters
        $('#task1').val(dataFull);
      });

  });
  $('#task2').bind('paste',function()
  {
      setTimeout(function()
      { 
        //get the value of the input text
        var data= $( '#task2' ).val() ;
        //replace the special characters to '' 
        var dataFull = data.replace(/,/g, '');
        //set the new value of the input text without special characters
        $('#task2').val(dataFull);
      });

  });
  $('#task3').bind('paste',function()
  {
      setTimeout(function()
      { 
        //get the value of the input text
        var data= $( '#task3' ).val() ;
        //replace the special characters to '' 
        var dataFull = data.replace(/,/g, '');
        //set the new value of the input text without special characters
        $('#task3').val(dataFull);
      });

  });
  $('#task4').bind('paste',function()
  {
      setTimeout(function()
      { 
        //get the value of the input text
        var data= $( '#task4' ).val() ;
        //replace the special characters to '' 
        var dataFull = data.replace(/,/g, '');
        //set the new value of the input text without special characters
        $('#task4').val(dataFull);
      });

  });
  $('#task5').bind('paste',function()
  {
      setTimeout(function()
      { 
        //get the value of the input text
        var data= $( '#task5' ).val() ;
        //replace the special characters to '' 
        var dataFull = data.replace(/,/g, '');
        //set the new value of the input text without special characters
        $('#task5').val(dataFull);
      });

  });
  });

  $('#task1, #task2, #task3, #task4, #task5').on("paste", function(e){
   var ingnore_key_codes = [188];
   if ($.inArray(e.keyCode, ingnore_key_codes) >= 0){
      e.preventDefault();
   }
  });

  $(document).ready(function(){
    $('.alert-success').fadeIn().delay(1000).fadeOut();
  });
</script>
</body>
</html>