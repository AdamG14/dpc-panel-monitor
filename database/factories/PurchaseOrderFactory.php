<?php

use App\Entity\PurchaseOrder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;

$factory->define(PurchaseOrder::class, function (Faker $faker) {
    $dateNow = Carbon::now()->format('Y-m-d');
    static $number = 1;
    return [
        'number' => $faker->asciify('***'),
        'value' => $faker->numberBetween(50, 200),
        'date' => $dateNow,
        'project_id' => $number++,
    ];
});
