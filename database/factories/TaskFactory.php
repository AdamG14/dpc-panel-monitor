<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entity\Task;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    static $number = 1;
    static $number2 = 1;
    $task = str_random(10).','.str_random(10).','.str_random(10);
    return [
        'detail'        => $task,
        'done'          => null,
        'user_id'       => $number2,
        'project_id'    => $number+=1,
    ];
});
