<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">

    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
      <form action="{{ route('updateProgress', $tasks->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="box box-primary">
            <div class="box-header">
                <h4 style="text-align: center;">Check your finished task!</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label>Location</label>
                    <input type="text" class="form-control" value="{{ $tasks->project->location }}" readonly>
                </div>
                <div class="form-group">
                    <label>Project Date</label>
                    <input type="text" class="form-control" value="{{ \Carbon\Carbon::parse($tasks->project->start_project)->format('d F Y') }} - {{ \Carbon\Carbon::parse($tasks->project->end_project)->format('d F Y') }}" readonly>
                </div>
                <div class="form-group">
                @foreach($task as $key => $t)
                    <div class="form-check form-check-inline">
                        <input type="checkbox" name="task{{$key}}" id="" class="form-check-input" style="width: 20px; height: 20px;" @if(!empty($done)) @foreach($done as $no => $d) {{ $t === $d ? 'checked' : ''}} @endforeach @endif>
                        <label class="form-check-label" style="font-size: 18px;">{{ $t }}</label>
                    </div>          
                    <input type="hidden" name="taskdetail{{$key}}" value="{{ $t }}">
                @endforeach
                </div>
                <button type="submit" class="btn btn-primary btn-block">Submit</button>
            </div>
        </form>            
        </div>
      </div>  
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
</body>
</html>