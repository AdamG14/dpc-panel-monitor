<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/bower_components/admin-lte/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>DPC</b> Dashboard</a>
  </div>

  @if (\Session::has('alert-logout'))
  <div class="alert alert-danger alert-dismissible">
    <div>
      {{Session::get('alert-logout')}}
      <a href="{{ url('login') }}"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
    </div>
  </div>
  @endif
  @if (\Session::has('alert-fail'))
  <div class="alert alert-danger alert-dismissible">
    <div>
      {{Session::get('alert-fail')}}
      <a href="{{ url('login') }}"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
    </div>
  </div>
  @endif

  <div class="login-box-body" style="height: 240px;">
    <p class="login-box-msg">Sign in to start manage data</p>

    <form action="{{ route('checkLogin')}}" method="post">
    {{ csrf_field() }}

      <div class="form-group has-feedback">

        <input type="email" class="form-control" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        @if($errors->has('email'))
        <div class="text-danger">
            {{ $errors->first('email')}}
        </div>
        @endif

      </div>
      <div class="form-group has-feedback">

        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if($errors->has('password'))
        <div class="text-danger">
            {{ $errors->first('password')}}
        </div>
        @endif

      </div>
      <div class="row">

        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat" style="margin-top: 15px;">Sign In</button>
        </div>
        
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</body>
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    $('.alert-danger').fadeIn().delay(1000).fadeOut();
  });
</script>
</html>
