<?php

Route::get('', 'AuthController@indexLogin')->name('login');
Route::post('login/check', 'AuthController@checkLogin')->name('checkLogin');
Route::get('logout', 'AuthController@logout');

Route::prefix('admin')->middleware('auth')->group(function () { 
    Route::get('main', 'AuthController@indexDashboard')->name('dashboard');
    Route::prefix('project')->group(function () {
        Route::get('list', 'ProjectController@index')->name('listProject');
        Route::get('create', 'ProjectController@create')->name('createProject');
        Route::post('store', 'ProjectController@store')->name('storeProject');
        Route::get('edit/{id}', 'ProjectController@edit')->name('editProject');
        Route::put('update/{id}', 'ProjectController@update')->name('updateProject');
        Route::get('delete/{id}', 'ProjectController@destroy')->name('deleteProject');
    });

    Route::prefix('task')->group(function () {        
        Route::get('list-employee', 'TaskController@indexEmployee')->name('listEmployeeTask');
        Route::get('list', 'TaskController@index')->name('listTask');
        Route::get('done', 'TaskController@indexDone')->name('doneTask');
        Route::post('store/{id}', 'TaskController@store')->name('storeTask');
        Route::get('{id}', 'TaskController@show')->name('userTask');
        Route::get('progress/{id}', 'TaskController@edit')->name('progressTask');
        Route::post('progress/{id}', 'TaskController@update')->name('updateProgress');
    });

    Route::get('document/{id}', 'ProjectController@show')->name('showDocument');
    Route::post('store/{id}', 'ProjectController@storeDocument')->name('storeDocument');

    Route::prefix('purchase-order')->group(function () {
        Route::get('list', 'PurchaseOrderController@index')->name('listPO');
    });
    Route::prefix('user')->group(function () {
        Route::get('list', 'UserController@index')->name('listUser');
        Route::get('create', 'UserController@create')->name('createUser');
        Route::post('store', 'UserController@store')->name('storeUser');
        Route::get('edit/{id}', 'UserController@edit')->name('editUser');
        Route::put('update/{id}', 'UserController@update')->name('updateUser');
        Route::get('delete/{id}', 'UserController@destroy')->name('deleteUser');
    });

    Route::get('list-notes', 'UserController@userNotes')->name('userNotes');

    Route::prefix('notes')->group(function() {
        Route::get('list', 'NotesController@index')->name('listNotes');
        Route::post('store/{id}', 'NotesController@store')->name('notesStore');
    });
    Route::get('log', 'ProjectController@indexLog')->name('logProject');
});

