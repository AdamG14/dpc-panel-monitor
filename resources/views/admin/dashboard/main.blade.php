<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard <small>{{ Carbon\Carbon::now()->format('d F Y')}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> DPC</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">PO chart this year</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h4>Value of PT. Dua Putra Cemerlang Utama</h4>
              <h3>Rp. {{ number_format($ptValue, 0, "", ".") }}</h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h4>Value of CV. Dua Putra Cemerlang</h4>
              <h3>Rp. {{ number_format($cvValue, 0, "", ".") }}</h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{ count($project) }}</h3>

              <p>Total Project</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-gear"></i>
            </div>
            <a href="{{ route('listProject') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ count($payment) }}</h3>

              <p>Total Payment</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-cart"></i>
            </div>
            <a href="{{ route('listPO') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ count($notes) }}</h3>

              <p>Total Notes</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-paper"></i>
            </div>
            <a href="{{ route('listNotes') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Current Project</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
              <th class="col-md-6">Location</th>
              <th class="col-md-3">Start</th>
              <th class="col-md-3">Deadline</th>
            </tr>
            </thead>
            <tbody>
              @foreach($project as $key => $p)
                @if($key < 5)
                <tr>
                    <td>{{ $p->location }}</td>
                    <td>{{ \Carbon\Carbon::parse($p->start_project)->format('d F Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($p->end_project)->format('d F Y') }}</td>
                </tr>
                @endif
              @endforeach
            </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Current Payment</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="col-md-4">Number</th>
                <th class="col-md-4">Value</th>
                <th class="col-md-4">Date</th>
              </tr>
            </thead>
            <tbody>
              @foreach($payment as $key => $p)
                @if($key < 13)
                <tr>
                    <td>{{ $p->number }}</td>
                    <td>{{ $p->value }}</td>
                    <td>{{ \Carbon\Carbon::parse($p->date)->format('d F Y') }}</td>
                </tr>
                @endif
              @endforeach
            </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Current Notes</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th class="col-md-3">Location</th>
                    <th class="col-md-3">User</th>
                    <th class="col-md-3">Title</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($notes as $key => $n)
                    @if($key < 3)
                    <tr>
                        <td>{{ $n->project->location }}</td>
                        <td>{{ $n->user->name }}</td>
                        <td>{{ $n->title }}</td>
                    </tr>
                    @endif
                  @endforeach
                </tbody>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
  </section>
    <section class="content-header" style="margin-top: -2%;">
      <h1>
        Project Status
      </h1>
    </section>
    <section class="content container-fluid" style="margin-bottom: -5%;">
      <div class="row">
        <div class="col-md-4">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Done</span>
              <span class="info-box-number">{{ count($done) }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: {{ 100 / count($project) * count($done) }}%"></div>
              </div>
                  <span class="progress-description">
                    {{ substr(100 / count($project) * count($done), 0, 2) }}% of project are done
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-spinner"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">On Progress</span>
              <span class="info-box-number">{{ count($progress) }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: {{ 100 / count($project) * count($progress) }}%"></div>
              </div>
                  <span class="progress-description">
                  {{ substr(100 / count($project) * count($progress), 0, 2) }}% of project are in progress
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pending</span>
              <span class="info-box-number">{{ count($pending) }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: {{ 100 / count($project) * count($pending) }}%"></div>
              </div>
                  <span class="progress-description">
                  {{ substr(100 / count($project) * count($pending), 0, 2) }}% of project are pending
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
    </section>
    <section class="content-header">
      <h1>
        User Activity
      </h1>
    </section>
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-3">
          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="ion ion-ios-eye"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Guest Visitor</span>
              <span class="info-box-number" style="font-size: 30px;">{{ $guest }}</span>

              <span class="progress-description">
                    Visits Today
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Employee Activity</span>
              <span class="info-box-number" style="font-size: 30px;">{{ $employee }}</span>

              <span class="progress-description">
                    Activity Today
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Admin Activity</span>
              <span class="info-box-number" style="font-size: 30px;">{{ $admin }}</span>

              <span class="progress-description">
                  Activity Today
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-user-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Directur Activity</span>
              <span class="info-box-number" style="font-size: 30px;">{{ $director }}</span>

              <span class="progress-description">
                  Activity Today
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
    </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/bower_components/chart.js/Chart.js"></script>
<script>
  $(function () {
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      datasets: [
        {
          label               : 'Electronics',
          fillColor           : '#337ab7',
          strokeColor         : '#337ab7',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [
            {{ json_encode($ptValueJan) }}, 
            {{ json_encode($ptValueFeb) }},
            {{ json_encode($ptValueMar) }}, 
            {{ json_encode($ptValueApr) }}, 
            {{ json_encode($ptValueMei) }}, 
            {{ json_encode($ptValueJun) }}, 
            {{ json_encode($ptValueJul) }}, 
            {{ json_encode($ptValueAgu) }}, 
            {{ json_encode($ptValueSep) }}, 
            {{ json_encode($ptValueOkt) }}, 
            {{ json_encode($ptValueNov) }}, 
            {{ json_encode($ptValueDes) }},  
            ]
        },
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [
            {{ json_encode($cvValueJan) }}, 
            {{ json_encode($cvValueFeb) }},
            {{ json_encode($cvValueMar) }}, 
            {{ json_encode($cvValueApr) }}, 
            {{ json_encode($cvValueMei) }}, 
            {{ json_encode($cvValueJun) }}, 
            {{ json_encode($cvValueJul) }}, 
            {{ json_encode($cvValueAgu) }}, 
            {{ json_encode($cvValueSep) }}, 
            {{ json_encode($cvValueOkt) }}, 
            {{ json_encode($cvValueNov) }}, 
            {{ json_encode($cvValueDes) }},  
            ]
        }
      ]
    }
    barChartData.datasets[1].fillColor   = '#00a65a'
    barChartData.datasets[1].strokeColor = '#00a65a'
    barChartData.datasets[1].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
  })
</script>

</body>
</html>