<?php

use Illuminate\Database\Seeder;
use App\Entity\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Project::class, 10)->create();
    }
}
