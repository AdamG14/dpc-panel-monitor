<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location')->nullable();
            $table->string('project_owner')->nullable();
            $table->enum('deliver_to', ['PT. Dua Putra Cemerlang', 'CV. Dua Putra Cemerlang'])->nullable();
            $table->date('start_project')->nullable();
            $table->date('end_project')->nullable();
            $table->date('atp')->nullable();
            $table->date('start_bast')->nullable();
            $table->date('end_bast')->nullable();
            $table->date('invoice_date')->nullable();
            $table->date('payment_date')->nullable();
            $table->enum('status', ['Pending', 'Progress', 'Done']);
            $table->string('image_sipil')->nullable();
            $table->string('image_perijinan')->nullable();
            $table->string('image_electrical')->nullable();
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->timestamps();
            $table->date('deleted_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
