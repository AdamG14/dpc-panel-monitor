<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
            All {{Auth::user()->name}}'s Task
      </h1>
      @if (Session::has('alert'))
      <div class="alert alert-success alert-dismissible" style="margin-bottom: -10px; margin-top: 10px;">
          <a href="admin/project/list"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert')}}
      </div>
      @endif
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-tasks"></i> DPC</a></li>
        <li>Dashboard</li>
        <li class="active">All {{Auth::user()->name}}'s Task</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-hover">
                  <thead>
                  <style>th{text-align: center;}</style>
                  <tr>
                    <th>Id</th>
                    <th>Status</th>
                    <th class="col-md-4">Location</th>
                    <th class="col-md-4">Detail</th>
                    <th class="col-md-4">Done</th>
                    <th>Progress</th> 
                    <th></th>
                  </tr>
                  </thead>          
                  <tbody>
                  @foreach($task as $key => $task)
                  <tr>
                    <td>{{ $task->id }}</td>
                    @if(!is_null($task->done) && count(explode(',', $task->done)) === count(explode(',', $task->detail)))
                      <td><span class="label label-success">Done</span></td>
                    @endif
                    @if(!is_null($task->done) && count(explode(',', $task->done)) != count(explode(',', $task->detail)))
                      <td><span class="label label-primary">Progress</span></td>
                    @endif
                    @if(is_null($task->done))
                      <td><span class="label label-danger">Pending</span></td>
                    @endif
                    <td>{{ $task->project['location'] }}</td>
                    <td>{{ $task->detail }}</td>
                    <td>{{ $task->done }}</td>
                    @if(!is_null($task->done) && count(explode(',', $task->done)) === count(explode(',', $task->detail)))
                      <td style="text-align: center; color: green; font-weight: bold;">{{ count(explode(',', $task->done)) }}/{{count(explode(',', $task->detail))}}</td>
                    @endif
                    @if(!is_null($task->done) && count(explode(',', $task->done)) != count(explode(',', $task->detail)))
                      <td style="text-align: center; color: blue; font-weight: bold;">{{ count(explode(',', $task->done)) }}/{{count(explode(',', $task->detail))}}</td>
                    @endif
                    @if(is_null($task->done))
                      <td style="text-align: center; color: red; font-weight: bold;">0 /{{count(explode(',', $task->detail))}}</td>
                    @endif
                    <td>
                    <a href="{{ route('progressTask', $task->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </table>
                </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
    $('#example1').DataTable( {
      "columnDefs" : [
        { 'visible': false, 'targets': [0] },
        { 'orderable' : false, 'targets': [1, 2, 3, 4, 5, 6]}
      ],
      "order": [[ 0, "desc" ]]
    });
  })
  
  $(document).ready(function(){
    $('.alert-success').fadeIn().delay(1000).fadeOut();
  });
</script>
</body>
</html>