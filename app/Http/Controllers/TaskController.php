<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Entity\Task;
use App\Entity\User;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task = Task::orderBy('id', 'DESC')->get();

        return view('admin.task.main')->with(compact(['task']));
    }
    
    public function indexEmployee()
    {
        $users = User::all();
        $task = Task::all();
        $userTasks = [];
        $eachTask = 0;
        $pendingTask = 0;
        $progressTask = 0;
        $doneTask  = 0;
        foreach($users as $key => $user) {            
            foreach($task as $t) {
                if($t->user_id == $user->id) {
                    $eachTask +=1;
                    if(is_null($t->done)) $pendingTask +=1;
                    if(!is_null($t->done) && $t->done != $t->detail) $progressTask +=1;
                    if(!is_null($t->done) && $t->done == $t->detail) $doneTask +=1;
                }
            }
            $userTasks[$user->id]['total']      = $eachTask;
            $userTasks[$user->id]['pending']    = $pendingTask;
            $userTasks[$user->id]['progress']   = $progressTask;
            $userTasks[$user->id]['done']       = $doneTask;
            $userTasks[$user->id]['overall']    = $doneTask. '/' .$eachTask;
            $eachTask                           = 0;
            $pendingTask                        = 0;
            $progressTask                       = 0;
            $doneTask                           = 0;
        }



        return view('admin.task.main')->with(compact(['userTasks', 'task', 'users']));
    }

    public function indexDone()
    {
        $task = Task::orderBy('id', 'DESC')->get();

        return view('admin.task.main')->with(compact(['task']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'task1' => 'required',
            'userId' => 'required',
        ]);

        $task = new Task;
        $task->detail       = $request->task1;
        if(!is_null($request->task2))$task->detail       = $task->detail .','. $request->task2;
        if(!is_null($request->task3))$task->detail       = $task->detail .','. $request->task3;
        if(!is_null($request->task4))$task->detail       = $task->detail .','. $request->task4;
        if(!is_null($request->task5))$task->detail       = $task->detail .','. $request->task5;
        $task->user_id      = $request->userId;
        $task->project_id   = $id;
        $task->save();
        
        return redirect()->back()->with('alert', 'Success add task');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->id == $id) {
            $task = Task::where('user_id', $id)->orderBy('updated_at', 'ASC')->get();
        
            return view('admin.task.user')->with(compact(['task']));
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tasks = Task::find($id);
        $task  = explode(',', $tasks->detail);
        $done  = is_null($tasks->done) ? null : explode(',', $tasks->done);
        
        return view('admin.task.progress', compact(['task', 'tasks', 'done']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $task->done = null;
        if($request->has('task0'))$task->done       = $request->taskdetail0;
        if($request->has('task1'))is_null($task->done) ? $task->done = $request->taskdetail1  : $task->done =  $task->done. ',' . $request->taskdetail1;
        if($request->has('task2'))is_null($task->done) ? $task->done = $request->taskdetail2  : $task->done =  $task->done. ',' . $request->taskdetail2;
        if($request->has('task3'))is_null($task->done) ? $task->done = $request->taskdetail3  : $task->done =  $task->done. ',' . $request->taskdetail3;
        if($request->has('task4'))is_null($task->done) ? $task->done = $request->taskdetail4  : $task->done =  $task->done. ',' . $request->taskdetail4;

        $task->save();
        return redirect()->route('userTask', Auth::user()->id)->with('alert', 'Progress submitted!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
