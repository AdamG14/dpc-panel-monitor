<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'detail', 'done', 'user_id', 'project_id'
    ];

    public function project()
    {
        return $this->belongsTo('App\Entity\Project', 'project_id', 'id')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo('App\Entity\User', 'user_id', 'id');
    }
}
