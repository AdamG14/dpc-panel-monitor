<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row"  style="margin-top: 4%;">
        <div class="col-md-4"></div>
        <div class="col-md-4">
        <form action="{{ route('updateUser', $user->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header" style="text-align: center;">
              <h3 class="box-title">Update Account</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label for="">Full Name</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}"autocomplete="off">
                @if($errors->has('name'))
                <div class="text-danger">
                    {{ $errors->first('name')}}
                </div>
                @endif
              </div>
              <div class="form-group">
                <label for="">Role</label>
                <select class="form-control" name="role">
                  <option value="{{ $user->role }}">{{ $user->role }}</option>
                  <option value="1">Employee</option>
                  <option value="2">Admin</option>
                  <option value="3">Directur</option>
                </select>
                @if($errors->has('role'))
                <div class="text-danger">
                    {{ $errors->first('role')}}
                </div>
                @endif
              </div>
              <div class="form-group">
                <label for="">Email</label>
                <input type="text" class="form-control" name="email" value="{{ $user->email }}" autocomplete="off">
                @if($errors->has('email'))
                <div class="text-danger">
                    {{ $errors->first('email')}}
                </div>
                @endif
              </div>
              <div class="form-group">
                <label for="">New Password</label>
                <input type="password" class="form-control" name="password" autocomplete="off">
                @if($errors->has('password'))
                <div class="text-danger">
                    {{ $errors->first('password')}}
                </div>
                @endif
              </div>
              <button type="submit" class="btn btn-primary" style="margin-left:41%;">Submit</button>
            </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
</body>
</html>