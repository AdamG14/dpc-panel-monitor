<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'number', 'value', 'date', 'project_id'
    ];

    public function project()
    {
        return $this->belongsTo('App\Entity\Project', 'project_id', 'id')->withTrashed();
    }
}
