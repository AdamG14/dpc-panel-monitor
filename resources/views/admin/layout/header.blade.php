    <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="{{ route('dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>DP</b>C</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Dua Putra</b> Cemerlang</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              @php 
                $task      = \App\Entity\Task::orderBy('updated_at', 'desc')->where('user_id', Auth::user()->id)->get();
                $taskongo  = 0;
                foreach($task as $t) {
                  $countTask  = count(explode(',', $t->detail));
                  $countDone  = is_null($t->done) ? 0 : count(explode(',', $t->done));
                  if($countDone != $countTask) {
                    $taskongo += 1;
                  }
                }
              @endphp
              <span class="label label-warning">{{ $taskongo }}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have {{ $taskongo }} notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                @foreach($task as $t)
                @php
                  $countTask  = count(explode(',', $t->detail));
                  $countDone  = is_null($t->done) ? 0 : count(explode(',', $t->done));
                @endphp
                @if($countDone != $countTask)
                  <li><!-- start notification -->
                    <a href="{{ route('progressTask', $t->id) }}">
                      <i class="fa fa-flag text-red"></i> You have {{ $countTask - $countDone }} task left in {{ $t->project['location'] }}
                    </a>
                  </li>
                  <!-- end notification -->
                @endif
                @endforeach
                </ul>
              </li>
              <!-- <li class="footer"><a href="#">View all</a></li> -->
            </ul>
          </li>
          <!-- Tasks Menu -->
          <li class="dropdown tasks-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              @php 
                $task      = \App\Entity\Task::orderBy('updated_at', 'desc')->where('user_id', Auth::user()->id)->get();
                $taskongo  = 0;
                foreach($task as $t) {
                  $countTask  = count(explode(',', $t->detail));
                  $countDone  = is_null($t->done) ? 0 : count(explode(',', $t->done));
                  if($countDone != $countTask) {
                    $taskongo += 1;
                  }
                }
              @endphp
              <span class="label label-danger">{{ $taskongo }}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have {{ $taskongo }} tasks</li>
              <li>
                <!-- Inner menu: contains the tasks -->
                <ul class="menu">
                @foreach($task as $t)
                @php
                  $countTask  = count(explode(',', $t->detail));
                  $countDone  = is_null($t->done) ? 0 : count(explode(',', $t->done));
                @endphp
                @if($countDone != $countTask)
                  <li><!-- Task item -->
                    <a href="{{ route('progressTask', $t->id) }}">
                      <!-- Task title and progress text -->
                      <h3>
                        {{ $t->project->location }}
                        <small class="pull-right">{{ substr(100 / $countTask * $countDone, 0, 4) }}%</small>
                      </h3>
                      <!-- The progress bar -->
                      <div class="progress xs">
                        <!-- Change the css width attribute to simulate progress -->
                        <div class="progress-bar progress-bar-aqua" style="width: {{ 100 / $countTask * $countDone }}%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                @endif
                @endforeach
                </ul>
              </li>
              <li class="footer">
                <a href="{{ route('userTask', Auth::user()->id) }}">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu" >
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span>{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header" style="margin-bottom: -19%;">  
                <p>
                  {{ Auth::user()->name }} - {{ Auth::user()->role }}                
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                @if(Auth::user()->role == 'Employee' || Auth::user()->role == 'Admin' || Auth::user()->role == 'Director')
                <div class="pull-left">
                  <a href="{{ route('userNotes') }}" class="btn btn-primary">Notes</a>
                </div>
                @endif
                <div class="pull-right">
                  <a href="{{ url('logout') }}" class="btn btn-danger"><i class="fa fa-sign-out"></i></a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->