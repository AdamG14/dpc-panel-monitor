<aside class="main-sidebar">

    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><b>MENU</b></li>
        <li class="{{ (request()->is('admin/main')) ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Director')
        <li class="{{ (request()->is('admin/project/*')) ? 'treeview active' : 'treeview' }}">
          <a href="#"><i class="fa fa-gear"></i> <span>Project</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (request()->is('admin/project/list')) ? 'active' : '' }}"><a href="{{ route('listProject') }}"><i class="fa fa-list"></i>Project List</a></li>
            @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Director')
            <li class="{{ (request()->is('admin/project/create')) ? 'active' : '' }}"><a href="{{ route('createProject') }}"><i class="fa fa-plus"></i>Add New Project</a></li>
            @endif
          </ul>
        </li>
        @elseif(Auth::user()->role == 'Employee' || Auth::user()->role == 'Guest')
        <li class="{{ (request()->is('admin/project/list')) ? 'active' : '' }}"><a href="{{ route('listProject') }}"><i class="fa fa-gear"></i>Project</a></li>
        @endif
        @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Director')
        <li class="{{ (request()->is('admin/user/*')) ? 'treeview active' : 'treeview' }}">
          <a href="#"><i class="fa fa-user"></i> <span>Account</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (request()->is('admin/user/list')) ? 'active' : '' }}"><a href="{{ route('listUser') }}"><i class="fa fa-list"></i> <span>Account List</span></a></li>
            <li class="{{ (request()->is('admin/user/create')) ? 'active' : '' }}"><a href="{{ route('createUser') }}"><i class="fa fa-plus"></i>Add New Account</a></li>
          </ul>
        </li>
        @endif
        @if(Auth::user()->role == 'Director')
        <li class="{{ (request()->is('admin/task/*')) ? 'treeview active' : 'treeview' }}">
          <a href="#"><i class="fa fa-tasks"></i> <span>Task</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (request()->is('admin/task/list')) ? 'active' : '' }}"><a href="{{ route('listTask') }}"><i class="fa fa-spinner"></i><span>Task on Progress</span></a></li>
            <li class="{{ (request()->is('admin/task/done')) ? 'active' : '' }}"><a href="{{ route('doneTask') }}"><i class="fa fa-check"></i>Task Done</a></li>
            <li class="{{ (request()->is('admin/task/list-employee')) ? 'active' : '' }}"><a href="{{ route('listEmployeeTask') }}"><i class="fa fa-users"></i>Employee Report</a></li>
          </ul>
        </li>
        @endif
        <li class="{{ (request()->is('admin/purchase-order/list')) ? 'active' : '' }}"><a href="{{ route('listPO') }}"><i class="fa fa-money"></i> <span>Purchase Order</span></a></li>
        <li class="{{ (request()->is('admin/notes/list')) ? 'active' : '' }}"><a href="{{ route('listNotes') }}"><i class="fa fa-book"></i> <span>Notes</span></a></li>
        <li><a href="#"><i class="fa fa-file"></i> <span>Storage</span></a></li>
        @if(Auth::user()->role == 'Director')
          <li class="{{ (request()->is('admin/log')) ? 'active' : '' }}"><a href="{{ route('logProject')}}"><i class="fa fa-history"></i>Log Action</a></li>          
        @endif
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>