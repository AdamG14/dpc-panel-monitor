<?php

use Illuminate\Database\Seeder;
use App\Entity\PurchaseOrder;

class PurchaseOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PurchaseOrder::class, 10)->create();
    }
}

