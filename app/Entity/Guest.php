<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $fillable = [
        'ip'
    ];
}
