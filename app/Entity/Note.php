<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'notes';

    protected $fillable = [
        'title', 'detail', 'date', 'project_id', 'user_id'
    ];

    public function project()
    {
        return $this->belongsTo('App\Entity\Project', 'project_id', 'id')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo('App\Entity\User', 'user_id', 'id');
    }
}
