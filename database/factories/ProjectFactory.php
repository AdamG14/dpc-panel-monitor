<?php

use App\Entity\Project;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;

$factory->define(Project::class, function (Faker $faker) {
    $dateNow = Carbon::now()->format('Y-m-d');
    $dateTomorrow = Carbon::tomorrow()->format('Y-m-d');
    return [
        'location' => $faker->address,
        'project_owner' => $faker->name,
        'deliver_to' => 1,
        'start_project' => $dateNow,
        'end_project' => $dateTomorrow,
        'atp' => $dateNow,
        'start_bast' => $dateNow,
        'end_bast' => $dateTomorrow,
        'invoice_date' => $dateNow,
        'payment_date' => $dateNow,
        'status' => $faker->numberBetween(1, 3),
        'user_id' => 1,
    ];
});
