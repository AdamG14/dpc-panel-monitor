<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
  <style>
  td{
    font-family: 'PT Sans', sans-serif;
    font-size: 15px;
    border: 2px solid #f4f4f4;
    padding: 10px;
  }
  
  @media (min-width: 1080px) {
    .table {
    width: 64%;
    }

    #tableRight{
      margin-top: -260px;
      width: 35%;
      float: right;
    }

    #tablePurchase{
      margin-top: 25px;
    }

    .button-group{
      margin-top: 20px;
      float: right;
      margin-right: 15px;
      margin-bottom: 20px;
    }

    #type-show{
      width:430px;  
    }

    #upload-mobile{
      display: none;
    }
  }

  @media (max-width: 1080px) {
    #tableRight{
      margin-top: 30px;
    }

    #tablePurchase{
      margin-top: 30px;
    }

    .button-group{
      margin-top: 20px;
      float: right;
      margin-bottom: 20px;
    }

    .button-group button{
      position: absolute;
      right: 55px;
    }

    #type-show{
      width:270px;  
      position: absolute;
      left: 10px;
    }

    #upload-pc{
      display: none;
    }
  }
  
  .value{
    float: right;
  }
 
  </style>
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Project
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> DPC</a></li>
        <li>Dashboard</li>
        <li>Project</li>
        <li class="active">Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    @if(Auth::user()->role == 'Employee' || Auth::user()->role == 'Admin' || Auth::user()->role == 'Director')
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
            <a href="{{ route('listProject') }}" class="btn" style="margin-bottom: 5px;"><i class="fa fa-arrow-left"></i></a>
            <table class="table table-responsive">
              <tr><td>Name <div class="value">{{ $project->location }}</div></td></tr>
              <tr><td>Owner <div class="value">{{ $project->project_owner }}</div></td></tr>
              <tr><td>Deliver to <div class="value">{{ $project->deliver_to }}</div></td></tr>
            </table>
            <table id="tablePurchase" class="table table-respsonsive">
            <div class="row">
              <tr><td>PO Number <div class="value">{{ $purchase->number }}</div></td></tr>
              <tr><td>PO Value <div class="value">Rp. {{ $purchase->value }}</div></td></tr>
              <tr><td>PO Date <div class="value">{{ \Carbon\Carbon::parse($purchase->date)->format('d F Y') }}</div></td></tr>
            </div>
            </table>

            <!-- Table Right -->
            <table id="tableRight" class="table table-respsonsive">
            <div class="row"></div>
              <tr><td>Start <div class="value">{{ \Carbon\Carbon::parse($project->start_project)->format('d F Y') }}</div></td></tr>
              <tr><td>Deadline <div class="value">{{ is_null($project->end_project) ? '' : \Carbon\Carbon::parse($project->end_project)->format('d F Y') }}</div></td></tr>
              <tr><td>Status <div class="value">
              @if($project->status == 'Pending')
              <span class="label label-danger">{{ $project->status }}</span></td>
              @elseif($project->status == 'Progress')
              <span class="label label-primary">{{ $project->status }}</span></td>
              @else
              <span class="label label-success">{{ $project->status }}</span></td>
              @endif
              </div></td></tr>
              <tr><td>ATP Date <div class="value">{{ is_null($project->atp) ? '' : \Carbon\Carbon::parse($project->atp)->format('d F Y') }}</div></td></tr>
              <tr><td>BAST Submit <div class="value">{{ is_null($project->start_bast) ? '' : \Carbon\Carbon::parse($project->start_bast)->format('d F Y') }}</div></td></tr>
              <tr><td>BAST Finish <div class="value">{{ is_null($project->end_bast) ? '' : \Carbon\Carbon::parse($project->end_bast)->format('d F Y') }}</div></td></tr>
              <tr><td>Invoice Date <div class="value">{{ is_null($project->invoice_date) ? '' : \Carbon\Carbon::parse($project->invoice_date)->format('d F Y') }}</div></td></tr>
              <tr><td>Payment Date <div class="value">{{ is_null($project->payment_date) ? '' : \Carbon\Carbon::parse($project->payment_date)->format('d F Y') }}</div></td></tr>
            </table>
            <div class="button-group">
              <form action="{{ route('showDocument', $project->id) }}" method="get" class="form-inline" >
                  <select name="typeShow" id="type-show" class="form-control">
                    <option value="">Select type document</option>
                    <option value="sipil">Sipil</option>
                    <option value="electrical">Electrical</option>
                    <option value="perijinan">Perijinan</option>
                  </select>
                  <button type="submit" class="btn btn-primary" id="search-button"><i class="fa fa-search"></i></button>
                  <a href="" id="upload-pc"class="btn btn-info" data-toggle="modal" data-target="#uploadDocument" onclick="upload_document('{{ $project->id }}')"><i class="fa fa-upload"></i></a>
              </form>
              <a href="" id="upload-mobile" class="btn btn-info btn-block" data-toggle="modal" data-target="#uploadDocument" onclick="upload_document('{{ $project->id }}')"><i class="fa fa-upload"></i></a>
            </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @endif
      <div class="row">
        <div class="col-xs-12">
        @if($imagesSipil[0] == "" && $imagesElectrical[0] == ""  && $imagesPerijinan[0] == "" )
        <h1 class="notice">There's no photo yet<h1>
        @else 
        @if(!$imagesSipil[0] == "")
          @foreach($imagesSipil as $image)
            <img src="{{ asset('images/sipil/'. $image) }}" alt="Not Found" width="295px" height="200px" style="object-fit: cover; padding: 10px;">
          @endforeach
        @endif   
        @if(!$imagesElectrical[0] == "")
          @foreach($imagesElectrical as $image)
            <img src="{{ asset('images/electrical/'. $image) }}" alt="Not Found" width="295px" height="200px" style="object-fit: cover; padding: 10px;">
          @endforeach
        @endif   
        @if(!$imagesPerijinan[0] == "")
          @foreach($imagesPerijinan as $image)
            <img src="{{ asset('images/perijinan/'. $image) }}" alt="Not Found" width="295px" height="200px" style="object-fit: cover; padding: 10px;">
          @endforeach
        @endif
        @endif  
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->

  <div class="modal fade" id="uploadDocument" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 30%; border-radius: 20px;">
      <div class="modal-header" style="background-color: #337ab7;">
        <h1 class="modal-title" style="text-align: center; font-family: 'PT Sans', sans-serif; color: white;">Upload Document</h1>
      </div>
      <div class="modal-body">
      <!-- Form Upload -->
      <form method="post" id="form_upload_document" enctype="multipart/form-data">
        {{ csrf_field() }}
          <div class="form-group">
            <label for="">Upload Here</label>
            <input name="photos[]" type="file" class="form-control" accept="image/*" multiple> 
            @if($errors->has('photos'))
            <div class="text-danger">
                {{ $errors->first('photos')}}
            </div>
            @endif
          </div>
          <div class="form-group">
            <select name="tipe" id="" class="form-control">
              <option value="">Select type document</option>
              <option value="sipil">Sipil</option>
              <option value="electrical">Electrical</option>
              <option value="perijinan">Perijinan</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary btn-block">Upload</button>
        </form>
      </div>
    </div>
  </div>
</div>
            
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
function upload_document(id){
    $("#form_upload_document").attr("action", "/admin/store/" + id);
}
</script>
</body>
</html>